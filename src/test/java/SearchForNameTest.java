

import org.junit.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import pageobjects.HomePage;
import sun.jvm.hotspot.utilities.Assert;

public class SearchForNameTest extends HomePage{

    private HomePage homePage;
    private String searchedName = "Rindlisbacher";


    @Test
    public void setUp(){
        homePage = new HomePage();
        homePage.openPage();
    }

    @Test(dependsOnMethods = "setUp")
    public void shallFindResults(){
        //given
        homePage.fillWhoInput(searchedName);
        //when
        homePage.clickSearchButton();
        //then
        int resultsSize =homePage.allResultsSize();
        Assert.that(resultsSize>750 && resultsSize<850, "Number of results is not in between 750 and 850");
    }


}
