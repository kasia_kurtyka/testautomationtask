import org.junit.Assert;
import org.testng.annotations.Test;
import pageobjects.HomePage;

public class CheckPhoneNumberTest extends HomePage {

    private HomePage homePage;

    private String city = "Zuerich";
    private String searchedName = "Grendelmeier Peter";
    private String phoneNumber = "044 433 02 55)";


    @Test
    public void setUp() {
        homePage = new HomePage();
        homePage.openPage();

    }

    @Test(dependsOnMethods = "setUp")
    public void shallSearchForNameAndCity() {

        //given
        homePage.fillWhoInput(searchedName)
                //when
                .fillWhereInput(city)
                .clickSearchButton();
        //then
        Assert.assertTrue("Phone number was not found", homePage.isPhoneNumberVisible(phoneNumber));

    }
}
