import org.testng.annotations.Test;
import pageobjects.HomePage;
import sun.jvm.hotspot.utilities.Assert;

/**
 * Created by katarzynakurtyka on 04.06.2018.
 */
public class CheckEmptyResultsTest extends HomePage {

    private HomePage homePage;

    private String city = "St. Gallen";
    private String searchedName = "Grendelmeier Hansueli";


    @Test
    public void setUp() {
        homePage = new HomePage();
        homePage.openPage();

    }

    @Test(dependsOnMethods = "setUp")
    public void shallFindCustomerByCityAndName(){

        //given
        homePage.fillWhoInput(searchedName)
                //when
                .fillWhereInput(city)
                .clickSearchButton();
        //then
        int resultsSize =homePage.allResultsSize();
        Assert.that(resultsSize==0, "No match is retrieved");

    }
}
