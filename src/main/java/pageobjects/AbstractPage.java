package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AbstractPage {

    public WebDriver driver;
    private static final String ADDRESS = "https://tel.search.ch/index.en.html";


    public void openPage() {
        System.setProperty("webdriver.chrome.driver", "/Users/katarzynakurtyka/projekt/drivers/Chrome/chromedriver_mac");
        driver = new ChromeDriver();
        driver.get(ADDRESS);
    }

    public  WebElement findElementBy(By by) {
        return driver.findElement(by);
    }
    public  void clickElement(By by) {
        findElementBy(by).click();
    }

    public void fillInput(By by, String text) {
        clickElement(by);
        findElementBy(by).sendKeys(text);
    }

    public  void fillInput(String xpath, String text) {
        fillInput(By.xpath(xpath), text);
        }

    public  WebElement findElementByXpath(String xpath) {
        return findElementBy(By.xpath(xpath));
    }
    public  void click(String xpath) {
        findElementByXpath(xpath).click();
    }


    }



