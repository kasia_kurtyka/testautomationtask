package pageobjects;


import org.openqa.selenium.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class HomePage extends AbstractPage {


    private static final Logger LOG = LoggerFactory.getLogger(HomePage.class);
    private static final String ADDRESS = "https://tel.search.ch/index.en.html";
    private static final String WHO_WHAT_INPUT = "//input[@name='was']";
    private static final String WHERE_INPUT = "//input[@name='wo']";
    private static final String SEARCH_BTN = "//div[@class='tel-submit-col']//input[@type='submit']";
    private static final String ROW_ACTION_BUTTON = "//a[@class='tel-result-action']";
    private static final String NUMBER = "//div[@class='tel-number']//a[contains(text(),'";
    private static final int LIST_ROW_SIZE = 110;
    private static final int MIN_SIZE_OF_RESULTS = 750;

    public HomePage fillWhoInput(String searchedName){
        fillInput(WHO_WHAT_INPUT, searchedName);
        return this;
    }

    public HomePage fillWhereInput(String city){
        fillInput(WHERE_INPUT,city);
        return this;
    }

    public HomePage clickSearchButton(){
        click(SEARCH_BTN);
        return this;
    }

    public int allResultsSize(){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        int maxListHeight= LIST_ROW_SIZE *MIN_SIZE_OF_RESULTS;
        int jumpHeight = LIST_ROW_SIZE*10;

        for(int i = 0; i<maxListHeight; i=i+jumpHeight) {
            js.executeScript("window.scrollBy(0,"+jumpHeight+")");
            try {
                Thread.sleep(600);
            }  catch (InterruptedException e) {
                LOG.debug("InterruptedException"+ e.toString());
        }
        }
        List<WebElement> resultRows = driver.findElements(By.xpath(ROW_ACTION_BUTTON));

        return resultRows.size();
    }

    public boolean isPhoneNumberVisible(String number){
        try {
            WebElement webElement = findElementByXpath("//div[@class='tel-number']//a[contains(text(),'" + number + "')]");
            return webElement.isDisplayed();

        } catch (NoSuchElementException e){
            return false;
        }
    }
}


